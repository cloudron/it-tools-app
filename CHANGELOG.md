[0.1.0]
* Initial version for IT-tools

[1.0.0]
* Initial stable release

[1.1.0]
* Add optional sso

[1.1.1]
* Update IT-Tools to v2023.8.16-9bd4ad4
* [Full changelog](https://github.com/CorentinTh/it-tools/releases/tag/v2023.8.16-9bd4ad4)
* Case Converter: Add lowercase and uppercase (#534) (7b6232a)
* new tool: emoji picker (#551) (93f7cf0)
* ui: added c-select in the ui lib (#550) (dfa1ba8)
* new-tool: password strength analyzer (#502) (a9c7b89)
* new-tool: yaml to toml (e29b258)
* new-tool: json to toml (ea50a3f)
* new-tool: toml to yaml (746e5bd)
* new-tool: toml to json (c7d4f11)
* command-palette: random tool action (ec4c533)
* config: allow app to run in a subfolder via `BASE_URL` (#461) (6304595)
* new-tool: percentage calculator (#456) (b9406a4)
* new-tool: json to csv converter (69f0bd0)
* new tool: xml formatter (#457) (a6bbeae)
* chmod-calculator: added symbolic representation (#455) (f771e7a)
* enhancement: use system dark mode (#458) (cf7b1f0)
* phone-parser: searchable country code select (d2956b6)
* new tool: camera screenshot and recorder (34d8e5c)
* base64-string-converter: switch to encode and decode url safe base64 strings (#392) (0b20f1c)

[1.2.0]
* Update IT-Tools to 2023.8.21-6f93cba
* [Full changelog](https://github.com/CorentinTh/it-tools/releases/tag/v2023.8.21-6f93cba)
* copy: support legacy copy to clipboard for older browser (#581) (6f93cba)
* new tool: string obfuscator (#575) (c58d6e3)

[1.3.0]
* Update base image to 4.2.0

[1.4.0]
* Update IT-Tools to 2023.11.1-e164afb
* [Full changelog](https://github.com/CorentinTh/it-tools/releases/tag/v2023.11.1-e164afb)
* command-palette: clear prompt on palette close (#708) (d013696)
* command-palette: added about page in command palette (99b1eb9)
* new tool: random MAC address generator (#657) (cc3425d)
* case-converter: added mocking case (#705) (681f7bf)
* date-converter: added excel date time format (#704) (f5eb7a8)
* i18n: token generator (#688) (02e68d3)
* i18n: home page (#687) (00562ed)
* i18n: support for i18n in .ts files (#683) (ebb4ec4)
* i18n: tool card (#682) (84a4a64)
* i18n: about page (#680) (a2b53c2)
* i18n: 404 page (#679) (35563b8)
* new tool: text to ascii converter (#669) (b2ad4f7)
* new tool: ULID generator (#623) (5c4d775)
* new tool: add wifi qr code generator (#599) (0eedce6)
* new tool: iban validation and parser (#591) (3a63837)
* new tool: text diff and comparator (#588) (81bfe57)

[1.5.0]
* Update IT-Tools to v2023.11.2-7d94e11
* [Full changelog](https://github.com/CorentinTh/it-tools/releases/tag/v2023.11.2-7d94e11)
* i18n: language selector

[1.5.1]
* Update IT-Tools to 2023.12.21-5ed3693
* [Full changelog](https://github.com/CorentinTh/it-tools/releases/tag/v2023.12.21-5ed3693)
* i18n: improve chinese i18n (#757) (2e56641)
* i18n: add tooltip and favoriteButton i18n (#756) (a1037cf)
* i18n: add Chinese translation base (#718) (8f99eb6)
* new tool: pdf signature checker (#745) (4781920)
* new tool: numeronym generator (#729) (e07e2ae)
* jwt-parser: jwt claim array support (#799) (5ed3693)
* camera-recorder: stop camera on navigation (#782) (80e46c9)
* doc: updated create new tool command in readme (#762) (7a70dbb)
* base64-file-converter: fix downloading of index.html content without data preambula (#750) (043e4f0)
* eta: corrected example (#737) (821cbea)
* about, i18n: improved i18n dx with markdown (#753) (bd3edcb)
* token, i18n: complete fr translation (#752) (de1ee69)
* uuid generator: uuid version picker (#751) (38586ca)
* case converter: no split on lowercase, uppercase and mocking case (#748) (ca43a25)
* ui: replaced legacy n-upload with c-file-upload (#747) (7fe47b3)
* token: added password in token generator keywords (#746) (16ffe6b)
* bcrypt: fix input label align (#721) (093ff31)

[1.6.0]
* Update IT-Tools to 2024.5.13-a0bc346
* [Full changelog](https://github.com/CorentinTh/it-tools/releases/tag/v2024.5.13-a0bc346)
* i18n: added German translation (#1038) (2c2fb21)
* new tool: Outlook Safelink Decoder (#911) (d3b32cc)
* new tool: ascii art generator (#886) (fe349ad)
* i18n: get locales on build (#880) (dc04615)
* i18n: added vi tools translations (#876) (079aa21)
* i18n: added zh tools translations (#874) (9c6b122)
* i18n: added missing locale files in tools (#863) (7f5fa00)
* i18n: added vietnamese language (#859) (1334bff)
* i18n: added spanish language (#854) (85b50bb)
* i18n: added portuguese language (#813) (c65ffb6)
* i18n: added ukrainian language (#827) (693f362)
* new-tool: yaml formater (#779) (fc06f01)
* new-tool: added unicode conversion utilities (#858) (c46207f)

[1.6.1]
* Update IT-Tools to 2024.10.22-7ca5933
* [Full changelog](https://github.com/CorentinTh/it-tools/releases/tag/v2024.10.22-7ca5933)

