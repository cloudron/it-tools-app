#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 50000;

    let browser, app;
    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TIMEOUT);
    }

    async function login({ proceed = true, alreadyLoggedIn = true }) {
        await browser.get(`https://${app.fqdn}`);

        if (proceed) {
            await waitForElement(By.id('loginProceedButton'));
            await browser.findElement(By.id('loginProceedButton')).click();
        }

        if (!alreadyLoggedIn) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();
        }

        await browser.wait(until.elementLocated(By.xpath('//h3[text()="All the tools"]')), TIMEOUT);
    }

    async function getMainPage() {
        await browser.get('https://' + app.fqdn);
        await browser.sleep(2000);
        await browser.wait(until.elementLocated(By.xpath('//h3[text()="All the tools"]')));
    }

    async function checkTokenGenerator() {
        await browser.get('https://' + app.fqdn + '/token-generator');
        await browser.sleep(2000);
        await browser.wait(until.elementLocated(By.xpath('//h1[text()="Token generator"]')));
    }

    async function checkDateTimeConverter() {
        await browser.get('https://' + app.fqdn + '/date-converter');
        await browser.sleep(2000);
        await browser.wait(until.elementLocated(By.xpath('//h1[text()="Date-time converter"]')));
    }

    async function checkURLEncoder() {
        await browser.get('https://' + app.fqdn + '/url-encoder');
        await browser.sleep(2000);
        await browser.wait(until.elementLocated(By.xpath('//h1[contains(text(), "Encode/decode")]')));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can login', login.bind(null, { proceed: true, alreadyLoggedIn: false }));

    it('can get the main page', getMainPage);

    it('can get Token Generator', checkTokenGenerator);
    it('can get Date Time Converter', checkDateTimeConverter);
    it('can get URL Encoder', checkURLEncoder);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', async function () {
        await browser.get('about:blank');
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
        // wait when all services are up and running
        await browser.sleep(20000);
    });
    it('can get app information', getAppInfo);
    it('can login', login.bind(null, { proceed: true, alreadyLoggedIn: true }));
    it('can get the main page', getMainPage);
    it('can get Token Generator', checkTokenGenerator);
    it('can get Date Time Converter', checkDateTimeConverter);
    it('can get URL Encoder', checkURLEncoder);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    it('install app (no sso)', function () { execSync(`cloudron install --no-sso --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can get the main page', getMainPage);
    it('can get Token Generator', checkTokenGenerator);
    it('can get Date Time Converter', checkDateTimeConverter);
    it('can get URL Encoder', checkURLEncoder);
    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app for update', function () { execSync(`cloudron install --appstore-id tech.ittools.cloudron --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can login', login.bind(null, { proceed: false, alreadyLoggedIn: true }));
    it('can get the main page', getMainPage);

    it('can get Token Generator', checkTokenGenerator);
    it('can get Date Time Converter', checkDateTimeConverter);
    it('can get URL Encoder', checkURLEncoder);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });

    it('can get Token Generator', checkTokenGenerator);
    it('can get Date Time Converter', checkDateTimeConverter);
    it('can get URL Encoder', checkURLEncoder);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});

