FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

# renovate: datasource=github-releases depName=CorentinTh/it-tools versioning=regex:^(?<major>\d+)\.(?<minor>\d+)\.(?<patch>\d+)-(?<build>.+)$ extractVersion=^v(?<version>.+)$
ARG ITTOOLS_VERSION=2024.10.22-7ca5933

RUN mkdir -p /app/code
WORKDIR /app/code

RUN cd /tmp && \
    wget https://github.com/CorentinTh/it-tools/releases/download/v${ITTOOLS_VERSION}/it-tools-${ITTOOLS_VERSION}.zip && \
    unzip it-tools-${ITTOOLS_VERSION} -d /app/code && \
    rm /tmp/it-tools-${ITTOOLS_VERSION}.zip

# Add nginx config
RUN rm /etc/nginx/sites-enabled/*
RUN ln -sf /dev/stdout /var/log/nginx/access.log
RUN ln -sf /dev/stderr /var/log/nginx/error.log
COPY nginx/readonlyrootfs.conf /etc/nginx/conf.d/readonlyrootfs.conf
COPY nginx/it-tools-nginx.conf /etc/nginx/sites-enabled/it-tools-nginx.conf

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
