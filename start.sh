#!/bin/bash

set -eu

echo "=> Starting IT Tools"
exec /usr/sbin/nginx -g "daemon off;"
